// Importing Modules
var express = require('express');
var mongoose = require('mongoose');
var bodyparser = require('body-parser')
var cors = require('cors');
var path = require('path');

var app = express();

const route = require('./routes/route');

// Connect To Mongodb
mongoose.connect('mongodb://localhost:27017/fms');

// On Connection
mongoose.connection.on('connected', ()=>{
    console.log('Connected to database mongodb @ 27017');
});

mongoose.connection.on('error', (err)=>{
    if(err){
        console.log('Error in Database Connection: ' + err);
    }
});


// Port Number
const port = 3000;

// Adding middleware - cors
app.use(cors());

// body-parser
app.use(bodyparser.json());

// Static Files
app.use(express.static(path.join(__dirname, 'client/dist')));

app.use('/api', route);

// Testing  
app.get('/', (req, res) => {
    res.send('foobar');
});

app.listen(port, () => {
    console.log('Server Started at port: ' + port);
});