const express = require('express');
const router = express.Router();


var courseController = require('../controllers/course');
var studentController = require('../controllers/students');


/* Course API's */
router.get('/course', courseController.getCourses);
router.post('/course', courseController.addCourse);
router.delete('/course/:id', courseController.deleteCourse);
router.put('/course/:id', courseController.updateCourse);
/* End of Course API's */


/* Students API's */
router.get('/students', studentController.getStudents);
router.post('/student', studentController.addStudent);
/* End of Students API's */

module.exports = router;