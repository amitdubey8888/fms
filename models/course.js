const mongoose = require('mongoose');

const CourseSchema = mongoose.Schema({
    name: {type: String, required: true},
    duration: {type: Number, require: true},
    fee: {type: Number, require: true},
    seats: {type: Number, require: true},
    firstYearFilledSeats: {type: Number}
});

const Course = module.exports = mongoose.model("Course", CourseSchema);