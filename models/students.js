const mongoose = require('mongoose');
const deepPopulate = require('mongoose-deep-populate')(mongoose);

const StudentSchema = mongoose.Schema({
    name: { type: String },
    fatherName: { type: String },
    motherName: { type: String },
    gender: { type: String },
    category: { type: String },
    subCategory: { type: String },
    domicile: { type: String },
    religion: { type: String },
    dob: { type: Date },
    mobile: { type: String },
    email: { type: String },
    state: { type: String },
    address: { type: String },
    district: { type: String },
    adhar: { type: String },
    fatherMobile: { type: String },
    fatherProfession: { type: String },
    academicDetails: [
        mongoose.Schema({
            exam: { type: String },
            rollNumber: { type: String },
            passingYear: { type: String },
            marksObtain: { type: String },
            totalMarks: { type: String },
            subjects: [{ type: String }],
            medium: { type: String }
        })
    ],
    course: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Course' }],
    subjects: [{ type: String }],
    year: { type: String },
    courseFee:
    mongoose.Schema({
        firstYear: { type: Number },
        secondYear: { type: Number },
        thirdYear: { type: Number }
    }),
    studentPay: mongoose.Schema({
        firstYear: { type: Number },
        secondYear: { type: Number },
        thirdYear: { type: Number }
    }),
    photo: { type: String },
    signature: { type: String }
},
{
    timestamps: true
});

StudentSchema.plugin(deepPopulate, {});

const Student = module.exports = mongoose.model("Student", StudentSchema);
