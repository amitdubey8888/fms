var Course = require('../models/course');

function addCourse(req, res, next) {
    let newCourse = new Course({
        name: req.body.name,
        duration: req.body.duration,
        fee: req.body.fee,
        seats: req.body.seats,
        firstYearFilledSeats: req.body.firstYearFilledSeats
    });
    newCourse.save((err, course) => {
        if (err) {
            res.json({ msg: 'Failed to add course!', data: newCourse});
        } else {
            res.json({ msg: 'Course Added successfully' });
        }
    });
}

function getCourses(req, res, next) {
    Course.find(function (err, courses) {
        res.json(courses);
    });
}

function deleteCourse(req, res, next) {
    Course.remove({ _id: req.params.id }, function (err, result) {
        if (err) {
            res.json(err);
        } else {
            res.json(result);
        }
    });
}

function updateCourse(req, res, next) {
    Course.findByIdAndUpdate(req.params.id, req.body, (err, result) => {
        if (err) {
            res.json(err);
        } else {
            res.json(result);
        }
    });
}

module.exports = {
    addCourse: addCourse,
    getCourses: getCourses,
    deleteCourse: deleteCourse,
    updateCourse: updateCourse
};