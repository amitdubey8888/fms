var Student = require('../models/students');

function addStudent(req, res, next) {
    let newStudent = new Student({
        name: req.body.name,
        fatherName: req.body.fatherName,
        motherName: req.body.motherName,
        gender: req.body.gender,
        category: req.body.category,
        subCategory: req.body.subCategory,
        domicile: req.body.domicile,
        religion: req.body.religion,
        dob: req.body.dob,
        mobile: req.body.mobile,
        email: req.body.email,
        state: req.body.state,
        address: req.body.address,
        district: req.body.district,
        adhar: req.body.adhar,
        fatherMobile: req.body.fatherMobile,
        fatherProfession: req.body.fatherProfession,
        academicDetails: req.body.academicDetails,
        course: req.body.course,
        subjects: req.body.subjects,
        year: req.body.year,
        courseFee: req.body.courseFee,
        studentPay: req.body.studentPay,
        photo: req.body.photo,
        signature: req.body.signature
    });

    newStudent.save((err, success) => {
        if (err) {
            res.json(err);
        } else {
            res.json(success);
        }
    });
}

function getStudents(req, res, next) {
    Student.find({}).deepPopulate('course').lean().exec(function (err, students) {
        if (err) {
            res.json(err);
        } else {
            res.json(students);
        }
    });
}

module.exports = {
    addStudent: addStudent,
    getStudents: getStudents
}