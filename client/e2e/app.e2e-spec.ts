import { FMSPage } from './app.po';

describe('fms App', () => {
  let page: FMSPage;

  beforeEach(() => {
    page = new FMSPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
