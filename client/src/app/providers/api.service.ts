import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Course } from '../models/course';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
  private URL = 'http://localhost:3000/api/';

  constructor(private http: Http) { }

  // Retrieving Courses
  getCourse() {
    return this.http.get(this.URL + 'course')
      .map(res => res.json());
  }

  // Add Course
  addCourse(newCourse) {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.URL + 'course', newCourse, { headers: headers })
      .map(res => res.json());
  }

  // Delete Course
  deleteCourse(id) {
    return this.http.delete(this.URL + 'course/' + id)
      .map(res => res.json());
  }

  // Update Course
  updateCourse(id, course) {
    console.log(id);
    console.log(course);
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put(this.URL + 'course/' + id, course, { headers: headers })
      .map(res => res.json());
  }

}
