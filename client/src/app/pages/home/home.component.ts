import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../providers/common.service';
import { ApiService } from '../../providers/api.service';
import { Course } from '../../models/course';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  courses: Course[];

  constructor(
    private common: CommonService,
    private api: ApiService
  ) {
    this.common.display.navbar = true;
    this.common.display.footer = true;
    this.common.display.copyright = true;
  }

  ngOnInit() {
    this.getCourse();
  }

  /* Get Courses */
  getCourse() {
    this.api.getCourse()
      .subscribe(courses => {
        console.info('Courses:', courses);
        this.courses = courses;
      });
  }



}
