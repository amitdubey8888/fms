import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from "@angular/forms";

import { CommonService } from '../../providers/common.service';
import { ApiService } from '../../providers/api.service';
import { Course } from '../../models/course';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {
  courses: Course[];
  course: Course = {
    _id: '',
    name: '',
    duration: 0,
    fee: 0,
    seats: 0,
    firstYearFilledSeats: 0
  };
  modal: any;

  constructor(
    private common: CommonService,
    private modalService: NgbModal,
    private api: ApiService
  ) {
    this.common.display.navbar = true;
    this.common.display.footer = true;
    this.common.display.copyright = true;
  }


  ngOnInit() {
    this.getCourses();
  }

  // Get Courses
  getCourses() {
    this.api.getCourse()
      .subscribe(courses => {
        this.courses = courses;
        console.log(this.courses);
      });
  }

  /* Add Course */
  addCourse() {
    this.api.addCourse(this.course)
      .subscribe(course => {
        this.modal.close();
        this.clearData();
        this.getCourses();
      });
  }

  /* Delete Course */
  deletecourse(id) {
    this.api.deleteCourse(id)
      .subscribe(res => {
        this.getCourses();
      });
  }

  /* Edit Course */
  editCourse(course, addCourseModal) {
    this.course = course;
    this.courseModalOpen(addCourseModal);
  }

  /* Update Course */
  updateCourse() {
    this.api.updateCourse(this.course._id, this.course)
      .subscribe(res => {
        this.modal.close();
        this.clearData();
        this.getCourses();
      })
  }

  /* Clear */
  clearData() {
    this.course = {
      _id: '',
      name: '',
      duration: 0,
      fee: 0,
      seats: 0,
      firstYearFilledSeats: 0
    };
  }


  courseModalOpen(addCourseModal) {
    this.modal = this.modalService.open(addCourseModal);
  }
}
