import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { CommonService } from '../../providers/common.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginDetails = {
    username: '',
    password: ''
  };
  constructor(
    private router:Router,
    private common: CommonService
  ) { }

  ngOnInit() {
  }

  login(){
    console.info('loginDetails:', this.loginDetails);
    this.router.navigate(['/home']);
  }

}
