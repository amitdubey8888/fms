export class Course {
    _id?: string;
    name: string;
    duration: number;
    fee: number;
    seats: number;
    firstYearFilledSeats?: number; 
}