export class Student {
    _id?: string;
    name: string;
    fatherName: string;
    motherName: string;
    gender: string;
    category: string;
    subCategory: string;
    domicile: string;
    religion: string;
    dob: Date;
    mobile: string;
    email: string;
    state: string;
    address: string;
    district: string;
    adhar: string;
    fatherMobile: string;
    fatherProfession: string;
    academicDetails: object;
    course: any;
    subjects: string;
    year: string;
    courseFee: string;
    studentPay: string;
    photo: string;
    signature: string;
}