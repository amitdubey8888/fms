webpackJsonp([1,5],{

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CommonService = (function () {
    function CommonService() {
        this.loginUser = null;
        this.display = {
            navbar: false,
            footer: false,
            copyright: false
        };
    }
    return CommonService;
}());
CommonService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [])
], CommonService);

//# sourceMappingURL=common.service.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_common_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service__ = __webpack_require__(60);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoursesComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CoursesComponent = (function () {
    function CoursesComponent(common, modalService, api) {
        this.common = common;
        this.modalService = modalService;
        this.api = api;
        this.course = {
            _id: '',
            name: '',
            duration: 0,
            fee: 0,
            seats: 0,
            firstYearFilledSeats: 0
        };
        this.common.display.navbar = true;
        this.common.display.footer = true;
        this.common.display.copyright = true;
    }
    CoursesComponent.prototype.ngOnInit = function () {
        this.getCourses();
    };
    // Get Courses
    CoursesComponent.prototype.getCourses = function () {
        var _this = this;
        this.api.getCourse()
            .subscribe(function (courses) {
            _this.courses = courses;
            console.log(_this.courses);
        });
    };
    /* Add Course */
    CoursesComponent.prototype.addCourse = function () {
        var _this = this;
        this.api.addCourse(this.course)
            .subscribe(function (course) {
            _this.modal.close();
            _this.clearData();
            _this.getCourses();
        });
    };
    /* Delete Course */
    CoursesComponent.prototype.deletecourse = function (id) {
        var _this = this;
        this.api.deleteCourse(id)
            .subscribe(function (res) {
            _this.getCourses();
        });
    };
    /* Edit Course */
    CoursesComponent.prototype.editCourse = function (course, addCourseModal) {
        this.course = course;
        this.courseModalOpen(addCourseModal);
    };
    /* Update Course */
    CoursesComponent.prototype.updateCourse = function () {
        var _this = this;
        this.api.updateCourse(this.course._id, this.course)
            .subscribe(function (res) {
            _this.modal.close();
            _this.clearData();
            _this.getCourses();
        });
    };
    /* Clear */
    CoursesComponent.prototype.clearData = function () {
        this.course = {
            _id: '',
            name: '',
            duration: 0,
            fee: 0,
            seats: 0,
            firstYearFilledSeats: 0
        };
    };
    CoursesComponent.prototype.courseModalOpen = function (addCourseModal) {
        this.modal = this.modalService.open(addCourseModal);
    };
    return CoursesComponent;
}());
CoursesComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-courses',
        template: __webpack_require__(253),
        styles: [__webpack_require__(241)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__providers_common_service__["a" /* CommonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_common_service__["a" /* CommonService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__providers_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_api_service__["a" /* ApiService */]) === "function" && _c || Object])
], CoursesComponent);

var _a, _b, _c;
//# sourceMappingURL=courses.component.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_common_service__ = __webpack_require__(10);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeeDetailsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FeeDetailsComponent = (function () {
    function FeeDetailsComponent(common, modalService) {
        this.common = common;
        this.modalService = modalService;
        this.studentDetails = {
            name: 'Jay Rana',
            fatherName: 'Ram Avatar Singh Rana',
            motherName: 'Kamlesh Devi',
            dob: new Date(),
            mobile: '+918385803337',
            email: 'jkrana008@gmail.com',
            class: 'B. Sc.',
            year: '2nd',
            rollNumber: '1234',
            admissionDate: new Date(),
            registration: '112342412'
        };
        this.common.display.navbar = true;
        this.common.display.footer = true;
        this.common.display.copyright = true;
    }
    FeeDetailsComponent.prototype.ngOnInit = function () {
    };
    FeeDetailsComponent.prototype.feeModalOpen = function (addFee) {
        this.modalService.open(addFee);
    };
    return FeeDetailsComponent;
}());
FeeDetailsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-fee-details',
        template: __webpack_require__(254),
        styles: [__webpack_require__(242)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__providers_common_service__["a" /* CommonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_common_service__["a" /* CommonService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ng_bootstrap_ng_bootstrap__["b" /* NgbModal */]) === "function" && _b || Object])
], FeeDetailsComponent);

var _a, _b;
//# sourceMappingURL=fee-details.component.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_common_service__ = __webpack_require__(10);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FeeComponent = (function () {
    function FeeComponent(common) {
        this.common = common;
        this.common.display.navbar = true;
        this.common.display.footer = true;
        this.common.display.copyright = true;
    }
    FeeComponent.prototype.ngOnInit = function () {
    };
    return FeeComponent;
}());
FeeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-fee',
        template: __webpack_require__(255),
        styles: [__webpack_require__(243)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */]) === "function" && _a || Object])
], FeeComponent);

var _a;
//# sourceMappingURL=fee.component.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_common_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service__ = __webpack_require__(60);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = (function () {
    function HomeComponent(common, api) {
        this.common = common;
        this.api = api;
        this.common.display.navbar = true;
        this.common.display.footer = true;
        this.common.display.copyright = true;
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.getCourse();
    };
    /* Get Courses */
    HomeComponent.prototype.getCourse = function () {
        var _this = this;
        this.api.getCourse()
            .subscribe(function (courses) {
            console.info('Courses:', courses);
            _this.courses = courses;
        });
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-home',
        template: __webpack_require__(256),
        styles: [__webpack_require__(244)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__providers_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_api_service__["a" /* ApiService */]) === "function" && _b || Object])
], HomeComponent);

var _a, _b;
//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_common_service__ = __webpack_require__(10);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = (function () {
    function LoginComponent(router, common) {
        this.router = router;
        this.common = common;
        this.loginDetails = {
            username: '',
            password: ''
        };
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function () {
        console.info('loginDetails:', this.loginDetails);
        this.router.navigate(['/home']);
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-login',
        template: __webpack_require__(257),
        styles: [__webpack_require__(245)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__providers_common_service__["a" /* CommonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__providers_common_service__["a" /* CommonService */]) === "function" && _b || Object])
], LoginComponent);

var _a, _b;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_common_service__ = __webpack_require__(10);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RegistrationComponent = (function () {
    function RegistrationComponent(common) {
        this.common = common;
        this.common.display.navbar = true;
        this.common.display.footer = true;
        this.common.display.copyright = true;
    }
    RegistrationComponent.prototype.ngOnInit = function () {
    };
    return RegistrationComponent;
}());
RegistrationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-registration',
        template: __webpack_require__(258),
        styles: [__webpack_require__(246)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */]) === "function" && _a || Object])
], RegistrationComponent);

var _a;
//# sourceMappingURL=registration.component.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_common_service__ = __webpack_require__(10);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentDetailsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StudentDetailsComponent = (function () {
    function StudentDetailsComponent(common) {
        this.common = common;
        this.studentDetails = {
            name: 'Jay Rana',
            fatherName: 'Ram Avatar Singh Rana',
            motherName: 'Kamlesh Devi',
            dob: new Date(),
            mobile: '+918385803337',
            email: 'jkrana008@gmail.com',
            class: 'B. Sc.',
            year: '2nd',
            rollNumber: '1234',
            admissionDate: new Date(),
            registration: '112342412'
        };
        this.common.display.navbar = true;
        this.common.display.footer = true;
        this.common.display.copyright = true;
    }
    StudentDetailsComponent.prototype.ngOnInit = function () {
    };
    return StudentDetailsComponent;
}());
StudentDetailsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-student-details',
        template: __webpack_require__(259),
        styles: [__webpack_require__(247)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */]) === "function" && _a || Object])
], StudentDetailsComponent);

var _a;
//# sourceMappingURL=student-details.component.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_common_service__ = __webpack_require__(10);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StudentsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StudentsComponent = (function () {
    function StudentsComponent(common) {
        this.common = common;
        this.students = [
            {
                name: 'Jay Rana',
                fatherName: 'Ram Avatar Singh Rana',
                motherName: 'Kamlesh Devi',
                dob: new Date(),
                mobile: '+918385803337',
                email: 'jkrana008@gmail.com',
                class: 'B. Sc.',
                year: '2nd',
                rollNumber: '1234',
                admissionDate: new Date()
            },
            {
                name: 'Jay Rana',
                fatherName: 'Ram Avatar Singh Rana',
                motherName: 'Kamlesh Devi',
                dob: new Date(),
                mobile: '+918385803337',
                email: 'jkrana008@gmail.com',
                class: 'B. Sc.',
                year: '2nd',
                rollNumber: '1234',
                admissionDate: new Date()
            }
        ];
        this.common.display.navbar = true;
        this.common.display.footer = true;
        this.common.display.copyright = true;
    }
    StudentsComponent.prototype.ngOnInit = function () {
    };
    return StudentsComponent;
}());
StudentsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-students',
        template: __webpack_require__(260),
        styles: [__webpack_require__(248)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */]) === "function" && _a || Object])
], StudentsComponent);

var _a;
//# sourceMappingURL=students.component.js.map

/***/ }),

/***/ 168:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 168;


/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(185);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_common_service__ = __webpack_require__(10);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(common) {
        this.common = common;
        this.title = 'app works!';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__(252),
        styles: [__webpack_require__(240)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__providers_common_service__["a" /* CommonService */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_common_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_service__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_routing__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home_component__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login_component__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_registration_registration_component__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_students_students_component__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_fee_fee_component__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_courses_courses_component__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_student_details_student_details_component__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_fee_details_fee_details_component__ = __webpack_require__(137);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_9__pages_home_home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_10__pages_login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_11__pages_registration_registration_component__["a" /* RegistrationComponent */],
            __WEBPACK_IMPORTED_MODULE_12__pages_students_students_component__["a" /* StudentsComponent */],
            __WEBPACK_IMPORTED_MODULE_13__pages_fee_fee_component__["a" /* FeeComponent */],
            __WEBPACK_IMPORTED_MODULE_14__pages_courses_courses_component__["a" /* CoursesComponent */],
            __WEBPACK_IMPORTED_MODULE_15__pages_student_details_student_details_component__["a" /* StudentDetailsComponent */],
            __WEBPACK_IMPORTED_MODULE_16__pages_fee_details_fee_details_component__["a" /* FeeDetailsComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */],
            __WEBPACK_IMPORTED_MODULE_8__app_routing__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot()
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_5__providers_common_service__["a" /* CommonService */], __WEBPACK_IMPORTED_MODULE_6__providers_api_service__["a" /* ApiService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 184:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_home_home_component__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_login_login_component__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_registration_registration_component__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_students_students_component__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_fee_fee_component__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_courses_courses_component__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_student_details_student_details_component__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_fee_details_fee_details_component__ = __webpack_require__(137);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var appRoutes = [
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_2__pages_home_home_component__["a" /* HomeComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_3__pages_login_login_component__["a" /* LoginComponent */] },
    { path: 'registration', component: __WEBPACK_IMPORTED_MODULE_4__pages_registration_registration_component__["a" /* RegistrationComponent */] },
    { path: 'students', component: __WEBPACK_IMPORTED_MODULE_5__pages_students_students_component__["a" /* StudentsComponent */] },
    { path: 'student-details', component: __WEBPACK_IMPORTED_MODULE_8__pages_student_details_student_details_component__["a" /* StudentDetailsComponent */] },
    { path: 'fee', component: __WEBPACK_IMPORTED_MODULE_6__pages_fee_fee_component__["a" /* FeeComponent */] },
    { path: 'fee-details', component: __WEBPACK_IMPORTED_MODULE_9__pages_fee_details_fee_details_component__["a" /* FeeDetailsComponent */] },
    { path: 'courses', component: __WEBPACK_IMPORTED_MODULE_7__pages_courses_courses_component__["a" /* CoursesComponent */] },
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: '**', redirectTo: '/home' }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */].forRoot(appRoutes)
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* RouterModule */]
        ]
    })
], AppRoutingModule);

//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 240:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Josefin+Sans);", ""]);

// module
exports.push([module.i, "\r\n\r\n\r\n\r\n/* Navbar */\r\n.FMS-navbar{\r\n  background: #fff;\r\n}\r\n.FMS-navbar a {\r\n  color: #222;\r\n  font-family: 'Josefin Sans', sans-serif;\r\n  letter-spacing: 1px;\r\n}\r\n.FMS-navbar-logo{\r\n  background: #001f3f;\r\n  color: #fff !important;\r\n  padding: 5px 10px;\r\n  border-radius: 10px;\r\n  width: 60px;\r\n  max-width: 100%;\r\n}\r\n.FMS-navbar-right{\r\n  width: 100px;\r\n  max-width: 100%;\r\n  margin-top: 5px !important;\r\n  text-align: right;\r\n}\r\n\r\n@media screen and (max-width: 991px){\r\n  .FMS-navbar-right{\r\n    text-align: left;\r\n  }\r\n}\r\n/* End of Navbar */\r\n\r\n\r\n/* Footer */\r\nfooter{\r\n  width: 100%;\r\n  background: #FFF;\r\n  color: #444;\r\n  padding: 30px 20px;\r\n}\r\nfooter ul{\r\n  list-style: none;\r\n  padding: 0;\r\n}\r\n.FMS-copyright{\r\n  background: #FFF;\r\n  color: #000;\r\n  padding: 20px;\r\n}\r\n.FMS-footer-logo{\r\n  background: #001f3f;\r\n  color: #FFF;\r\n  padding: 5px 10px;\r\n  border-radius: 10px;\r\n  width: auto;\r\n  font-size: 25px;\r\n  letter-spacing: 2px;\r\n  font-family: 'Josefin Sans', sans-serif !important;\r\n}\r\n.FMS-footer-description{\r\n  margin-top: 15px;\r\n  font-size: 12px;\r\n  text-align: justify;\r\n}\r\n.FMS-footer-heading{\r\n  font-family: \"Trebuchet MS\", Verdana, sans-serif;\r\n}\r\n.FMS-footer-links{\r\n  margin-top: 23px;\r\n  font-size: 13px;\r\n}\r\n.FMS-footer-address{\r\n  font-style: italic;\r\n  font-family: 'Playfair Display', serif;\r\n}\r\n.FMS-footer-contact{\r\n  font-size: 14px;\r\n}\r\n\r\n/* End of Footer */\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 241:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports


// module
exports.push([module.i, ".FMS-add-btn{\r\n  float: right;\r\n  cursor: pointer;\r\n}\r\n\r\n@media screen and (max-width: 600px){\r\n  .container-fluid{\r\n    padding:0;\r\n  }\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 242:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports


// module
exports.push([module.i, ".FMS-card-block{\r\n  font-size: 14px;\r\n}\r\n\r\n.FMS-profile-image{\r\n  width: 180px;\r\n  height: 150px;\r\n}\r\n\r\n.FMS-profile-signature{\r\n  width: 180px;\r\n  height: 60px;\r\n}\r\n\r\n\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 243:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 244:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Oswald);", ""]);
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Playfair+Display);", ""]);

// module
exports.push([module.i, ".FMS-home-container {\r\n  margin-top: 10px;\r\n}\r\n\r\n.card {\r\n  border-radius: 0;\r\n  border-top: 3px solid #e2e2e2;\r\n  margin-top: 20px;\r\n  margin-bottom: 20px;\r\n  box-shadow: 2px 2px 5px #144512;\r\n}\r\n\r\n.card-header {\r\n  background: #fff;\r\n  height: 30px;\r\n  padding: 4px 2px 0 8px;\r\n}\r\n\r\n.FMS-progress {\r\n  margin-top: 15px;\r\n}\r\n\r\n@media screen and (max-width: 600px) {\r\n  .FMS-add-btn {\r\n    margin-top: -40px;\r\n  }\r\n  .card-block{\r\n    text-align: center !important;\r\n  }\r\n  .FMS-add-btn{\r\n    margin: -6px 5px 0 0 !important;\r\n  }\r\n}\r\n\r\n.FMS-add-btn {\r\n  padding: 0 5px;\r\n  margin: -6px -15px 0 0;\r\n  font-size: 14px;\r\n}\r\n\r\n.FMS-class {\r\n  font-size: 16px;\r\n  font-family: 'Oswald', sans-serif;\r\n}\r\n\r\n.FMS-subtext{\r\n  font-size: 12px;\r\n  font-weight: bold;\r\n  letter-spacing: 1px;\r\n  font-family: 'Playfair Display', serif;\r\n}\r\n.FMS-maintext{\r\n  font-family: 'Oswald', sans-serif;\r\n}\r\n\r\n.FMS-main-heading{\r\n  letter-spacing: 2px;\r\n  color: #696969;\r\n  font-family: 'Oswald', sans-serif;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 245:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Exo);", ""]);
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Exo|Playfair+Display);", ""]);

// module
exports.push([module.i, ".FMS-login{\r\n  width: 300px;\r\n  max-width: 100%;\r\n  margin-top:60px;\r\n  font-family: 'Exo', sans-serif;\r\n\r\n}\r\n\r\n.FMS-logo{\r\n  font-size: 150px;\r\n  color: #787878;\r\n  font-family: 'Playfair Display', serif;\r\n}\r\n\r\n@media screen and (max-width: 420px){\r\n  .FMS-logo{\r\n    font-size: 135px;\r\n  }\r\n}\r\n.FMS-welcome{\r\n  letter-spacing: 3px;\r\n  font-size: 20px;\r\n}\r\n\r\n.FMS-description{\r\n  letter-spacing: 1px;\r\n  font-size: 12px;\r\n}\r\n\r\n.FMS-access{\r\n  font-size: 13px;\r\n}\r\n\r\n.FMS-input{\r\n  border-radius: 0;\r\n  font-size: 14px;\r\n}\r\n\r\n.FMS-login-btn{\r\n  border-radius: 0;\r\n  background-color: #001f3f;\r\n  color: #fff;\r\n  letter-spacing: 2px;\r\n  font-family: 'Playfair Display', serif;\r\n  cursor: pointer;\r\n}\r\n\r\n.FMS-forgot{\r\n  font-size: 11px;\r\n  margin-top: 15px;\r\n  letter-spacing: 1px;\r\n}\r\n\r\n.FMS-about{\r\n  font-family: cursive;\r\n  font-size: 12px;\r\n  color: #898989;\r\n  margin-top: 25px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 246:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports


// module
exports.push([module.i, ".FMS-input-label {\r\n  font-size: 14px;\r\n  letter-spacing: 1.5px;\r\n  font-weight: 600;\r\n}\r\n\r\n.FMS-input {\r\n  border-radius: 0;\r\n  font-size: 14px;\r\n  padding: 8px 10px !important;\r\n}\r\n\r\n\r\n.FMS-group{\r\n  margin-top:15px;\r\n  margin-bottom: 20px;\r\n}\r\n\r\n.FMS-group-heading {\r\n  font-size: 16px;\r\n  letter-spacing: 1px;\r\n  font-weight: 600;\r\n}\r\n\r\n.FMS-photo{\r\n  margin-top: 15px;\r\n  border: 1px solid grey;\r\n  height: 200px;\r\n  width: 180px;\r\n}\r\n\r\n.FMS-signature{\r\n  margin-top:15px;\r\n  border:1px solid grey;\r\n  height: 60px;\r\n  width: 300px;\r\n}\r\n\r\n@media screen and (max-width: 600px) {\r\n  .container-fluid {\r\n    padding: 0;\r\n  }\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 247:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports


// module
exports.push([module.i, ".FMS-card-block{\r\n  font-size: 14px;\r\n}\r\n\r\n.FMS-profile-image{\r\n  width: 180px;\r\n  height: 150px;\r\n}\r\n\r\n.FMS-profile-signature{\r\n  width: 180px;\r\n  height: 60px;\r\n}\r\n\r\n.FMS-profile-academy{\r\n  border: 1px solid #ccc;\r\n  padding: 5px 10px;\r\n  width: 100%;\r\n}\r\n\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 248:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(8)(false);
// imports


// module
exports.push([module.i, "tr{\r\n  font-size: 14px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 252:
/***/ (function(module, exports) {

module.exports = "<!--Nav bar-->\r\n<nav class=\"navbar navbar-toggleable-md navbar-light bg-faded FMS-navbar\" *ngIf=\"common.display.navbar\">\r\n  <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\"\r\n          data-target=\"#navbarTogglerDemo02\" aria-controls=\"navbarTogglerDemo02\" aria-expanded=\"false\"\r\n          aria-label=\"Toggle navigation\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n  <a class=\"navbar-brand FMS-navbar-logo\" routerLink=\"/home\">FMS</a>\r\n\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarTogglerDemo02\" style=\"float:right;\">\r\n    <ul class=\"navbar-nav my-2 mt-md-0 mr-auto\">\r\n      <li class=\"nav-item active\">\r\n        <a class=\"nav-link\" routerLink=\"/home\">Home <span class=\"sr-only\">(current)</span></a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/registration\">Registration</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/students\">Students</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/fee\">Fee</a>\r\n      </li>\r\n      <li class=\"nav-item\">\r\n        <a class=\"nav-link\" routerLink=\"/courses\">Courses</a>\r\n      </li>\r\n    </ul>\r\n    <div class=\"FMS-navbar-right  my-2 my-lg-0\">\r\n      <p><i class=\"fa fa-sign-out\"></i> Logout</p>\r\n    </div>\r\n  </div>\r\n\r\n\r\n</nav>\r\n<!-- End of Nav bar -->\r\n\r\n<div class=\"FMS-main\">\r\n  <router-outlet></router-outlet>\r\n  <!-- Routed views go here -->\r\n</div>\r\n\r\n\r\n\r\n<!-- Footer -->\r\n<footer *ngIf=\"common.display.footer\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4 col-sm-12\">\r\n      <span class=\"FMS-footer-logo\">FMS</span>\r\n      <p class=\"FMS-footer-description\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pellentesque\r\n        eros eget blandit dictum. Donec\r\n        ut nisi scelerisque, maximus eros ut, tempus tortor. Ut et turpis ullamcorper eros placerat porttitor in eget\r\n        sapien. Suspendisse scelerisque, dui vitae mollis semper, nisl leo interdum urna, sit amet egestas ipsum neque\r\n        vitae nisi. Fusce eget libero eleifend, rhoncus nunc ac, tempor lacus. Donec vulputate id quam id aliquam. </p>\r\n    </div>\r\n\r\n    <div class=\"col-md-4 col-sm-12\">\r\n      <h4 class=\"FMS-footer-heading\">Important Links</h4>\r\n      <ul class=\"FMS-footer-links\">\r\n        <li><a href=\"#\">About</a></li>\r\n        <li><a href=\"#\">Privacy Policy</a></li>\r\n        <li><a href=\"#\">Terms & Conditions</a></li>\r\n      </ul>\r\n    </div>\r\n\r\n    <div class=\"col-md-4 col-sm-12\">\r\n      <h4 class=\"FMS-footer-heading\">CONTACT US</h4>\r\n      <address class=\"FMS-footer-address\">\r\n        Team-3 Company,<br>\r\n        C-28, Shivalik Appartment,<br>\r\n        Sector-4 Indira Gandhi Nagar,<br>\r\n        Jagatpura, Jaipur - 302017\r\n      </address>\r\n      <p class=\"FMS-footer-contact\">\r\n        +918385803337<br>\r\n        +919627256850<br>\r\n        contact@unknown.com<br>\r\n      </p>\r\n\r\n    </div>\r\n  </div>\r\n</footer>\r\n<p class=\"text-center FMS-copyright\" *ngIf=\"common.display.copyright\">\r\n  &copy; 2017 Unknown Company\r\n</p>\r\n<!-- End of Footer -->\r\n"

/***/ }),

/***/ 253:
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"card FMS-card\">\r\n    <div class=\"card-header FMS-card-header\">\r\n      Courses\r\n      <button class=\"btn FMS-add-btn btn-primary\" (click)=\"courseModalOpen(addCourseModal)\"><i class=\"fa fa-plus\"></i></button>\r\n    </div>\r\n    <div class=\"card-block table-responsive\">\r\n      <table class=\"table table-bordered\">\r\n        <thead>\r\n          <tr>\r\n            <th>#</th>\r\n            <th>Name</th>\r\n            <th>Duration</th>\r\n            <th>Fee</th>\r\n            <th>Seats</th>\r\n            <th style=\"max-width:60px;\">Action</th>\r\n          </tr>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let course of courses; let i = index\">\r\n            <td>0{{i+1}}</td>\r\n            <td>{{course.name}}</td>\r\n            <td>{{course.duration}} Year</td>\r\n            <td>Rs {{course.fee}}</td>\r\n            <td>{{course.seats}}</td>\r\n            <td style=\"max-width:60px;\">\r\n              <button class=\"btn btn-link\" (click)=\"editCourse(course, addCourseModal)\" style=\"cursor:pointer\"><i class=\"fa fa-pencil\"></i></button>\r\n              &nbsp;&nbsp;&nbsp;\r\n              <button class=\"btn btn-link text-danger\" (click)=\"deletecourse(course._id)\" style=\"cursor:pointer\"><i class=\"fa fa-trash\"></i></button>\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n<!-- New Course Modal -->\r\n<ng-template #addCourseModal let-c=\"close\" let-d=\"dismiss\">\r\n  <div class=\"modal-header\">\r\n    <h4 class=\"modal-title\">Add New Course</h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <form #courseForm=\"ngForm\" (submit)=\"course._id ? updateCourse() : addCourse()\">\r\n\r\n    <div class=\"modal-body\">\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-xs-12 col-sm-12 col-md-3\">\r\n          <label class=\"form-control-label FMS-input-label\" for=\"name\">Name <span class=\"FMS-error-validation\">*</span>:</label>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-9\">\r\n          <input type=\"text\" name=\"name\" id=\"name\" class=\"form-control FMS-input\" [(ngModel)]=\"course.name\" #nameField=\"ngModel\" required>\r\n\r\n          <div *ngIf=\"nameField.errors && (nameField.dirty || nameField.touched)\" class=\"FMS-error-validation\">\r\n            <span [hidden]=\"!nameField.errors.required\">*required</span>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-xs-12 col-sm-12 col-md-3\">\r\n          <label class=\"form-control-label FMS-input-label\" for=\"seats\">Seats <span class=\"FMS-error-validation\">*</span>:</label>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-9\">\r\n          <input type=\"number\" min=\"0\" max=\"1000\" name=\"seats\" id=\"seats\" class=\"form-control FMS-input\" [(ngModel)]=\"course.seats\" #seatsField=\"ngModel\"\r\n            required>\r\n          <div class=\"FMS-error-validation\" *ngIf=\"seatsField.errors && (seatsField.dirty || seatsField.touched)\">\r\n            <span [hidden]=\"!seatsField.errors.required\">*required</span>\r\n            <span [hidden]=\"!seatsField.errors.min\">Seats should be +ve number</span>\r\n            <span [hidden]=\"!seatsField.errors.max\">Seats cannot be greater than 1000</span>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-xs-12 col-sm-12 col-md-3\">\r\n          <label class=\"form-control-label FMS-input-label\" for=\"fee\">Fee <span class=\"FMS-error-validation\">*</span></label>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-9\">\r\n          <input type=\"number\" name=\"fee\" id=\"fee\" class=\"form-control FMS-input\" [(ngModel)]=\"course.fee\" #feeField=\"ngModel\" required>\r\n          <div class=\"FMS-error-validation\" *ngIf=\"feeField.errors && (feeField.dirty || feeField.touched)\">\r\n            <span [hidden]=\"!feeField.errors.required\">*required</span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-xs-12 col-sm-12 col-md-3\">\r\n          <label class=\"form-control-label FMS-input-label\" for=\"duration\">Duration <span class=\"FMS-error-validation\">*</span>:</label>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-9\">\r\n          <input type=\"number\" name=\"duration\" id=\"duration\" class=\"form-control FMS-input\" [(ngModel)]=\"course.duration\" #durationField=\"ngModel\"\r\n            required>\r\n          <div class=\"FMS-error-validation\" *ngIf=\"durationField.errors && (durationField.dirty || durationField.touched)\">\r\n            <span [hidden]=\"!durationField.errors.required\">*required</span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n    <div class=\"modal-footer\">\r\n      <button type=\"button\" class=\"btn btn-secondary \" (click)=\"c('Close click')\">Close</button>\r\n      <button type=\"submit\"  class=\"btn btn-primary\" [disabled]=\"courseForm.invalid\">Save</button>\r\n    </div>\r\n  </form>\r\n</ng-template>\r\n<!-- End of Modal -->\r\n"

/***/ }),

/***/ 254:
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"card FMS-card\">\r\n    <div class=\"card-header FMS-card-header\">\r\n      Fee Details\r\n      <button type=\"button\" class=\"btn btn-primary\" style=\"float: right\" (click)=\"feeModalOpen(addFee)\">Add Fee</button>\r\n    </div>\r\n    <div class=\"card-block FMS-card-block container\">\r\n\r\n      <!-- Basic Details-->\r\n      <div class=\"row FMS-margin FMS-padding\" style=\"border:1px solid #c2c2c2;\">\r\n        <div class=\"col-sm-9\">\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 col-xs-6 FMS-input-label text-right\">Name :</div>\r\n                <div class=\"col-sm-9 col-xs-6\">{{studentDetails.name}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Father :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.fatherName}}</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Mother :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.motherName}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">DOB :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.dob | date: 'dd-MMM-yy'}}</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Reg. No :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.registration}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Roll No :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.rollNumber}}</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Mobile :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.mobile}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Email :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.email}}</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Class :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.class}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Year :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.year}} year</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n        <div class=\"col-sm-3\">\r\n          <div class=\"text-right\">\r\n            <img src=\"../../../assets/images/default_profile.png\" class=\"FMS-profile-image img-thumbnail\">\r\n            <img src=\"../../../assets/images/defualt_signature.png\" class=\"FMS-profile-signature\">\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- End of Basic Details-->\r\n\r\n      <!-- Fee Details -->\r\n      <div style=\"margin-top: 20px\">\r\n        <div>\r\n          <ngb-tabset>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>1st Year</b></ng-template>\r\n              <ng-template ngbTabContent>\r\n                <div class=\"row container\" style=\"margin-top:25px\">\r\n                  <div class=\"col-sm-4\"><b>Payed:</b> Rs 8000</div>\r\n                  <div class=\"col-sm-4 text-center\"><b>Remaining:</b> Rs 0</div>\r\n                  <div class=\"col-sm-4 text-right\"><b>Total Fee:</b> Rs8000</div>\r\n                </div>\r\n\r\n                <div class=\"table-responsive\" style=\"margin-top: 20px\">\r\n                  <table class=\"table table-bordered\">\r\n                    <thead>\r\n                    <tr>\r\n                      <th>#</th>\r\n                      <th>Date</th>\r\n                      <th>Fee</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                    <tr>\r\n                      <td>01</td>\r\n                      <td>14-July-2017</td>\r\n                      <td>Rs2500</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>02</td>\r\n                      <td>12-July-2017</td>\r\n                      <td>Rs3000</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>03</td>\r\n                      <td>12-June-2017</td>\r\n                      <td>Rs1500</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>04</td>\r\n                      <td>01-May-2017</td>\r\n                      <td>Rs 1000</td>\r\n                    </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>2nd Year</b></ng-template>\r\n              <ng-template ngbTabContent>\r\n                <div class=\"row container\" style=\"margin-top:25px\">\r\n                  <div class=\"col-sm-4\"><b>Payed:</b> Rs 8000</div>\r\n                  <div class=\"col-sm-4 text-center\"><b>Remaining:</b> Rs 0</div>\r\n                  <div class=\"col-sm-4 text-right\"><b>Total Fee:</b> Rs8000</div>\r\n                </div>\r\n\r\n                <div class=\"table-responsive\" style=\"margin-top: 20px\">\r\n                  <table class=\"table table-bordered\">\r\n                    <thead>\r\n                    <tr>\r\n                      <th>#</th>\r\n                      <th>Date</th>\r\n                      <th>Fee</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                    <tr>\r\n                      <td>01</td>\r\n                      <td>14-July-2017</td>\r\n                      <td>Rs2500</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>02</td>\r\n                      <td>12-July-2017</td>\r\n                      <td>Rs3000</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>03</td>\r\n                      <td>12-June-2017</td>\r\n                      <td>Rs1500</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>04</td>\r\n                      <td>01-May-2017</td>\r\n                      <td>Rs 1000</td>\r\n                    </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n        </div>\r\n      </div>\r\n      <!-- End of Fee Details -->\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n<!-- Add Fee Modal -->\r\n<ng-template #addFee let-c=\"close\" let-d=\"dismiss\">\r\n  <div class=\"modal-header\">\r\n    <h4 class=\"modal-title\">Add Fee</h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"modal-body\">\r\n    <form>\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-xs-12 col-sm-12 col-md-3\">\r\n          <label class=\"form-control-label FMS-input-label\" for=\"name\">Fee:</label>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-9\">\r\n          <input type=\"text\" name=\"name\" id=\"name\" class=\"form-control FMS-input\" placeholder=\"Enter Rupees\">\r\n        </div>\r\n      </div>\r\n    </form>\r\n  </div>\r\n  <div class=\"modal-footer\">\r\n    <button type=\"button\" class=\"btn btn-secondary \" (click)=\"c('Close click')\">Close</button>\r\n    <button type=\"submit\" class=\"btn btn-primary\">Save</button>\r\n  </div>\r\n</ng-template>\r\n<!-- End of Fee Modal -->\r\n"

/***/ }),

/***/ 255:
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"card FMS-card\">\r\n    <div class=\"card-header FMS-card-header\">Fee</div>\r\n    <div class=\"card-block\">\r\n\r\n      <!-- Filter -->\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <select name=\"class\" id=\"class\" class=\"form-control FMS-input\">\r\n            <option value=\"\">Select Class</option>\r\n            <option value=\"bsc\">B. Sc.</option>\r\n            <option value=\"ba\">B. A.</option>\r\n            <option value=\"ma\">M. A.</option>\r\n            <option value=\"bcom\">B. Com.</option>\r\n          </select>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <select name=\"year\" class=\"form-control FMS-input\" id=\"year\">\r\n            <option value=\"\">Select Year</option>\r\n            <option value=\"1\">1st Year</option>\r\n            <option value=\"2\">2nd Year</option>\r\n            <option value=\"3\">3rd Year</option>\r\n          </select>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <input type=\"text\" name=\"name\" id=\"name\" class=\"form-control FMS-input\" placeholder=\"Enter Student Name\">\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <input type=\"text\" name=\"father_name\" id=\"father_name\" class=\"form-control FMS-input\"\r\n                 placeholder=\"Enter Father Name\">\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <input type=\"text\" name=\"roll_number\" id=\"roll_number\" class=\"form-control FMS-input\"\r\n                 placeholder=\"Enter Roll Number\">\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <input type=\"text\" name=\"mobile\" id=\"mobile\" class=\"form-control FMS-input\" placeholder=\"Enter Mobile Number\">\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-sm-12 text-right\">\r\n          <button type=\"button\" class=\"btn btn-primary\">Search</button>\r\n        </div>\r\n      </div>\r\n      <!-- End of Filter -->\r\n\r\n      <hr>\r\n\r\n      <div class=\"table-responsive\">\r\n        <table class=\"table table-bordered\">\r\n          <thead>\r\n          <tr>\r\n            <th>#</th>\r\n            <th>Name</th>\r\n            <th>Father Name</th>\r\n            <th>Roll No</th>\r\n            <th>1st Year</th>\r\n            <th>2nd Year</th>\r\n            <th>3rd Year</th>\r\n            <th>Action</th>\r\n          </tr>\r\n          </thead>\r\n          <tbody>\r\n          <tr>\r\n            <td>01</td>\r\n            <td>Appu Rana</td>\r\n            <td>Anoop Rana</td>\r\n            <td>1311331</td>\r\n            <td>8000 - 8000 = 0</td>\r\n            <td>8000 - 5000 = 3000</td>\r\n            <td>8000 - 0 = 8000</td>\r\n            <td><button class=\"btn btn-primary\" routerLink=\"/fee-details\">Fee Details</button></td>\r\n          </tr>\r\n          <tr>\r\n            <td>01</td>\r\n            <td>Appu Rana</td>\r\n            <td>Anoop Rana</td>\r\n            <td>1311331</td>\r\n            <td>8000 - 8000 = 0</td>\r\n            <td>8000 - 5000 = 3000</td>\r\n            <td>8000 - 0 = 8000</td>\r\n            <td><button class=\"btn btn-primary\" routerLink=\"/fee-details\">Fee Details</button></td>\r\n          </tr>\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 256:
/***/ (function(module, exports) {

module.exports = "<div class=\"container FMS-home-container\">\r\n  <h4 class=\"text-center FMS-main-heading\">SEAT DETAILS</h4>\r\n  <div class=\"row\">\r\n    <div class=\"col-sm-12 col-md-6\" *ngFor=\"let course of courses\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-9 col-md-9\" style=\"max-width: 70%\">\r\n              <h4 class=\"FMS-class\">{{course.name}} 1st Year</h4>\r\n            </div>\r\n            <div class=\"col-sm-3 col-md-3 text-right\" style=\"max-width: 30%;\">\r\n              <button class=\"btn btn-primary FMS-add-btn\"><i class=\"fa fa-plus\"></i></button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"card-block\">\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-4 col-xs-4\" style=\"max-width: 33%\">\r\n              <h5 class=\"FMS-maintext\">{{course.firstYearFilledSeats}}</h5> <span class=\"FMS-subtext\">Students</span>\r\n            </div>\r\n            <div class=\"col-sm-4 col-xs-4 text-center\" style=\"max-width: 33%\">\r\n              <h5 class=\"FMS-maintext\">{{course.seats}}</h5> <span class=\"FMS-subtext\">Total Seats</span>\r\n            </div>\r\n            <div class=\"col-sm-4 col-xs-4 text-right\" style=\"max-width: 33%\">\r\n              <h5 class=\"FMS-maintext\">{{course.seats - course.firstYearFilledSeats}}</h5> <span class=\"FMS-subtext\">Seats Available</span>\r\n            </div>\r\n          </div>\r\n          <div class=\"progress FMS-progress\">\r\n            <div class=\"progress-bar\" role=\"progressbar\" style=\"width: 66%; height: 5px\" aria-valuenow=\"66\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 257:
/***/ (function(module, exports) {

module.exports = "<div class=\"container FMS-login text-center\">\r\n  <div>\r\n    <h1 class=\"FMS-logo\">FMS</h1>\r\n    <h5 class=\"FMS-welcome\">Welcome to FMS</h5>\r\n    <p class=\"FMS-description\">FMS is a fee management system for Colleges and Schools. It will manage student details as well as fee.</p>\r\n    <p class=\"FMS-access\">Login to access your account</p>\r\n  </div>\r\n  <div>\r\n    <form autocomplete=\"off\">\r\n      <div class=\"form-group\">\r\n        <input type=\"email\" placeholder=\"Username\" class=\"form-control FMS-input\" id=\"username\" name=\"username\" required>\r\n      </div>\r\n      <div class=\"form-group\">\r\n        <input type=\"password\" placeholder=\"Password\" class=\"form-control FMS-input\" id=\"password\" name=\"password\" required>\r\n      </div>\r\n      <div>\r\n        <button type=\"submit\" class=\"btn btn-block FMS-login-btn\">Login</button>\r\n      </div>\r\n    </form>\r\n    <p class=\"FMS-forgot\"><a href=\"#\">Forgot Password?</a></p>\r\n  </div>\r\n  <div>\r\n    <p class=\"FMS-about\">Fee Management System By Team3</p>\r\n  </div>\r\n</div>\r\n\r\n\r\n"

/***/ }),

/***/ 258:
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"\">\r\n    <div class=\"card FMS-card\">\r\n      <div class=\"card-header FMS-card-header\">\r\n        Registration\r\n      </div>\r\n      <div class=\"card-block\">\r\n        <form class=\"\">\r\n          <!-- Basic Details -->\r\n          <div class=\"FMS-group\">\r\n            <h5 class=\"FMS-group-heading\">Basic Information:</h5>\r\n            <hr>\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"name\">Name: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"name\" id=\"name\" class=\"form-control FMS-input\" placeholder=\"Name\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"fathername\">Father Name: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"fathername\" id=\"fathername\" class=\"form-control FMS-input\"\r\n                           placeholder=\"Father Name\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"mothername\">Mother Name: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"mothername\" id=\"mothername\" class=\"form-control FMS-input\"\r\n                           placeholder=\"Mother Name\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"gender-male\">Gender: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"radio\" name=\"gender\" id=\"gender-male\" class=\"\" value=\"male\"> Male\r\n                    <input type=\"radio\" name=\"gender\" id=\"gender-female\" class=\"\" value=\"female\"> Female\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"category\">Category: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <select id=\"category\" name=\"category\" class=\"form-control FMS-input\">\r\n                      <option value=\"ur\">UR</option>\r\n                      <option value=\"obc\">OBC</option>\r\n                      <option value=\"sc\">SC</option>\r\n                      <option value=\"st\">ST</option>\r\n                      <option value=\"def\">DEF</option>\r\n                      <option value=\"ue\">UE</option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"subcategory\">Sub-Category: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <select name=\"subcategory\" id=\"subcategory\" class=\"form-control FMS-input\">\r\n                      <option value=\"\">None</option>\r\n                      <option value=\"ph\">Physically Handicapped</option>\r\n                      <option value=\"freedom\">Freedom Fighter</option>\r\n                      <option value=\"ex\">Ex. Defence Personnel</option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"domicile\">Domicile: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"radio\" name=\"domicile\" id=\"domicile\" value=\"up\"> U.P.\r\n                    <input type=\"radio\" name=\"domicile\" id=\"domicile1\" value=\"nonup\"> Non - U.P.\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"religion\">Religion: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"religion\" id=\"religion\" class=\"form-control FMS-input\"\r\n                           placeholder=\"Religion\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n          <!-- End of Basic Details -->\r\n\r\n          <!-- Contact Details-->\r\n          <div class=\"FMS-group\">\r\n            <h5 class=\"FMS-group-heading\">Contact Details</h5>\r\n            <hr>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"dob\">Date of Birth: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"date\" name=\"dob\" id=\"dob\" class=\"form-control FMS-input\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"mobile\">Mobile: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"mobile\" id=\"mobile\" class=\"form-control FMS-input\"\r\n                           placeholder=\"Mobile Number\" maxlength=\"13\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"email\">Email: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"email\" name=\"email\" id=\"email\" class=\"form-control FMS-input\"\r\n                           placeholder=\"Email Address\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"state\">State: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"state\" id=\"state\" class=\"form-control FMS-input\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"address\">Address: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <textarea name=\"address\" id=\"address\" class=\"form-control FMS-input\" rows=\"4\"\r\n                              placeholder=\"Address\"></textarea>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"district\">District: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"district\" id=\"district\" class=\"form-control FMS-input\"\r\n                           placeholder=\"District Name\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"adhar\">Adhar Number: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"adhar\" id=\"adhar\" class=\"form-control FMS-input\" placeholder=\"Adhar number\"\r\n                           maxlength=\"12\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"fathernumber\">Father Mobile: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"fathernumber\" id=\"fathernumber\" class=\"form-control FMS-input\"\r\n                           placeholder=\"Father number\" maxlength=\"13\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"fprofession\">Father Profession: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"text\" name=\"fprofession\" id=\"fprofession\" class=\"form-control FMS-input\"\r\n                           placeholder=\"Father profession\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n          </div>\r\n          <!-- End of Contact Details-->\r\n\r\n          <!-- Academic Details -->\r\n          <div class=\"FMS-group\">\r\n            <h5 class=\"FMS-group-heading\">Academic Details</h5>\r\n            <hr>\r\n            <div class=\"row\" style=\"font-size: 14px;\">\r\n              <div class=\"col-sm-1\">Exam</div>\r\n              <div class=\"col-sm-2\">Roll Number</div>\r\n              <div class=\"col-sm-2\">Passing Year</div>\r\n              <div class=\"col-sm-1\">Marks Obt</div>\r\n              <div class=\"col-sm-1\">Total Marks</div>\r\n              <div class=\"col-sm-3\">Subjects</div>\r\n              <div class=\"col-sm-2\">Medium</div>\r\n            </div>\r\n\r\n            <div class=\"row\" style=\"font-size: 14px; margin-top:10px\">\r\n              <div class=\"col-sm-1\">\r\n                <select name=\"1oth\" id=\"10th\" class=\"form-control FMS-input\">\r\n                  <option value=\"10th\" selected>10th</option>\r\n                  <!--<option value=\"12th\">12th</option>-->\r\n                </select>\r\n              </div>\r\n              <div class=\"col-sm-2\">\r\n                <input type=\"text\" name=\"roll_number\" id=\"roll_number\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-2\">\r\n                <input type=\"text\" name=\"passing_year\" id=\"passing_year\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-1\">\r\n                <input type=\"text\" name=\"marks_obt\" id=\"marks_obt\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-1\">\r\n                <input type=\"text\" name=\"marks_total\" id=\"marks_total\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-3\">\r\n                <input type=\"text\" name=\"subjects\" id=\"subjects\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-2 radio\">\r\n                <input type=\"radio\" name=\"medium_1oth\" value=\"hindi\"> Hindi\r\n                <input type=\"radio\" name=\"medium_10th\" value=\"english\"> English\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\" style=\"font-size: 14px;margin-top:10px\">\r\n              <div class=\"col-sm-1\">\r\n                <select name=\"1oth\" id=\"10th\" class=\"form-control FMS-input\">\r\n                  <!--<option value=\"10th\" selected>10th</option>-->\r\n                  <option value=\"12th\" selected>12th</option>\r\n                </select>\r\n              </div>\r\n              <div class=\"col-sm-2\">\r\n                <input type=\"text\" name=\"roll_number\" id=\"roll_number\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-2\">\r\n                <input type=\"text\" name=\"passing_year\" id=\"passing_year\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-1\">\r\n                <input type=\"text\" name=\"marks_obt\" id=\"marks_obt\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-1\">\r\n                <input type=\"text\" name=\"marks_total\" id=\"marks_total\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-3\">\r\n                <input type=\"text\" name=\"subjects\" id=\"subjects\" class=\"form-control FMS-input\">\r\n              </div>\r\n              <div class=\"col-sm-2 radio\">\r\n                <input type=\"radio\" name=\"medium_1oth\" value=\"hindi\"> Hindi\r\n                <input type=\"radio\" name=\"medium_10th\" value=\"english\"> English\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <!-- End of Academic Details -->\r\n\r\n          <!-- Admission Details -->\r\n          <div class=\"FMS-group\">\r\n            <h5 class=\"FMS-group-heading\">Admission Details</h5>\r\n            <hr>\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"course\">Course: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <select name=\"course\" id=\"course\" class=\"form-control  FMS-input\">\r\n                      <option value=\"bsc\">B. SC.</option>\r\n                      <option value=\"ba\">B. A.</option>\r\n                      <option value=\"ma\">M. A.</option>\r\n                      <option value=\"bcom\">B. Com.</option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"subjects_course\">Subjects: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <textarea class=\"form-control  FMS-input\" name=\"subjects_course\" id=\"subjects_course\"></textarea>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"course_year\">Year: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <select name=\"course_year\" id=\"course_year\" class=\"form-control  FMS-input\">\r\n                      <option value=\"1\">1st Year</option>\r\n                      <option value=\"2\">2nd Year</option>\r\n                      <option value=\"3\">3rd Year</option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"course_fee_1\">Course Fee: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-3\">\r\n                    <input type=\"number\" name=\"course_fee\" id=\"course_fee_1\" class=\"form-control FMS-input\" placeholder=\"1st Year\">\r\n                  </div>\r\n                  <div class=\"col-sm-3\">\r\n                    <input type=\"number\" name=\"course_fee\" id=\"course_fee_2\" class=\"form-control FMS-input\" placeholder=\"2nd Year\">\r\n                  </div>\r\n                  <div class=\"col-sm-3\">\r\n                    <input type=\"number\" name=\"course_fee\" id=\"course_fee_3\" class=\"form-control FMS-input\" placeholder=\"3rd Year\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"course_pay_1\">Student Pay: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-3\">\r\n                    <input type=\"number\" name=\"course_pay_1\" id=\"course_pay_1\" placeholder=\"1st Year\" class=\"form-control FMS-input\">\r\n                  </div>\r\n                  <div class=\"col-sm-3\">\r\n                    <input type=\"number\" name=\"course_pay_1\" id=\"course_pay_2\" placeholder=\"2nd Year\" class=\"form-control FMS-input\">\r\n                  </div>\r\n                  <div class=\"col-sm-3\">\r\n                    <input type=\"number\" name=\"course_pay_1\" id=\"course_pay_3\" placeholder=\"3rd Year\" class=\"form-control FMS-input\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"photo\">Photo: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"file\" class=\"form-control FMS-input\" id=\"photo\" name=\"photo\">\r\n                    <div class=\"FMS-photo\">\r\n\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"col-xs-12 col-sm-12 col-md-6\">\r\n                <div class=\"form-group row\">\r\n                  <div class=\"col-sm-3 FMS-input-label\">\r\n                    <label for=\"signature\">Signature: </label>\r\n                  </div>\r\n                  <div class=\"col-sm-9\">\r\n                    <input type=\"file\" class=\"form-control FMS-input\" name=\"signature\" id=\"signature\">\r\n                    <div class=\"FMS-signature\">\r\n\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n          </div>\r\n          <!-- End of Admission Details -->\r\n\r\n        </form>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 259:
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"card FMS-card\">\r\n    <div class=\"card-header FMS-card-header\">Student Details</div>\r\n    <div class=\"card-block FMS-card-block container\">\r\n\r\n      <!-- Basic Details-->\r\n      <div class=\"row FMS-margin\">\r\n        <div class=\"col-sm-9\">\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 col-xs-6 FMS-input-label text-right\">Name :</div>\r\n                <div class=\"col-sm-9 col-xs-6\">{{studentDetails.name}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Father :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.fatherName}}</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Mother :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.motherName}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">DOB :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.dob | date: 'dd-MMM-yy'}}</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Reg. No :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.registration}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Roll No :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.rollNumber}}</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Mobile :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.mobile}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Email :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.email}}</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Class :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.class}}</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"col-sm-6\">\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3 FMS-input-label text-right\">Year :</div>\r\n                <div class=\"col-sm-9\">{{studentDetails.year}} year</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n        <div class=\"col-sm-3\">\r\n          <div class=\"text-right\">\r\n            <img src=\"../../../assets/images/default_profile.png\" class=\"FMS-profile-image img-thumbnail\">\r\n            <img src=\"../../../assets/images/defualt_signature.png\" class=\"FMS-profile-signature\">\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- End of Basic Details-->\r\n\r\n\r\n      <!-- Academic Details-->\r\n      <div style=\"margin-top: 20px\">\r\n        <h6>Academy Details</h6>\r\n        <hr style=\"border-color: green;\">\r\n        <div class=\"row\" style=\"font-size: 14px;\">\r\n          <div class=\"col-sm-1\">Exam</div>\r\n          <div class=\"col-sm-2\">Roll No</div>\r\n          <div class=\"col-sm-1\">Year</div>\r\n          <div class=\"col-sm-1\">Marks Obt</div>\r\n          <div class=\"col-sm-1\">Total Marks</div>\r\n          <div class=\"col-sm-3\">Subjects</div>\r\n          <div class=\"col-sm-1\">Medium</div>\r\n          <div class=\"col-sm-2\">Board</div>\r\n        </div>\r\n\r\n        <div class=\"row\" style=\"font-size: 14px; margin-top:10px\">\r\n          <div class=\"col-sm-1\">\r\n            <p class=\"FMS-profile-academy\">10th</p>\r\n          </div>\r\n          <div class=\"col-sm-2\">\r\n            <p class=\"FMS-profile-academy\">11233123</p>\r\n          </div>\r\n          <div class=\"col-sm-1\">\r\n            <p class=\"FMS-profile-academy\">2017</p>\r\n          </div>\r\n          <div class=\"col-sm-1\">\r\n            <p class=\"FMS-profile-academy\">500</p>\r\n          </div>\r\n          <div class=\"col-sm-1\">\r\n            <p class=\"FMS-profile-academy\">600</p>\r\n          </div>\r\n          <div class=\"col-sm-3\">\r\n            <p class=\"FMS-profile-academy\">Hindi, English, Math, Physics, Chemistry</p>\r\n          </div>\r\n          <div class=\"col-sm-1\">\r\n            <p class=\"FMS-profile-academy\">Hindi</p>\r\n          </div>\r\n          <div class=\"col-sm-2\">\r\n            <p class=\"FMS-profile-academy\">UP Board</p>\r\n          </div>\r\n        </div>\r\n        <div class=\"row\" style=\"font-size: 14px; margin-top:10px\">\r\n          <div class=\"col-sm-1\">\r\n            <p class=\"FMS-profile-academy\">12th</p>\r\n          </div>\r\n          <div class=\"col-sm-2\">\r\n            <p class=\"FMS-profile-academy\">11233123</p>\r\n          </div>\r\n          <div class=\"col-sm-1\">\r\n            <p class=\"FMS-profile-academy\">2017</p>\r\n          </div>\r\n          <div class=\"col-sm-1\">\r\n            <p class=\"FMS-profile-academy\">500</p>\r\n          </div>\r\n          <div class=\"col-sm-1\">\r\n            <p class=\"FMS-profile-academy\">600</p>\r\n          </div>\r\n          <div class=\"col-sm-3\">\r\n            <p class=\"FMS-profile-academy\">Hindi, English, Math, Physics, Chemistry</p>\r\n          </div>\r\n          <div class=\"col-sm-1 radio\">\r\n            <p class=\"FMS-profile-academy\">Hindi</p>\r\n          </div>\r\n          <div class=\"col-sm-2\">\r\n            <p class=\"FMS-profile-academy\">UP Board</p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- End of Academic Details -->\r\n\r\n      <!-- Fee Details -->\r\n      <div style=\"margin-top: 20px\">\r\n        <h6>Fee Details</h6>\r\n        <hr style=\"border-color: green\">\r\n        <div>\r\n          <ngb-tabset>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>1st Year</b></ng-template>\r\n              <ng-template ngbTabContent>\r\n                <div class=\"row container\" style=\"margin-top:25px\">\r\n                  <div class=\"col-sm-4\"><b>Payed:</b> Rs 8000</div>\r\n                  <div class=\"col-sm-4 text-center\"><b>Remaining:</b> Rs 0</div>\r\n                  <div class=\"col-sm-4 text-right\"><b>Total Fee:</b> Rs8000</div>\r\n                </div>\r\n\r\n                <div class=\"table-responsive\" style=\"margin-top: 20px\">\r\n                  <table class=\"table table-bordered\">\r\n                    <thead>\r\n                    <tr>\r\n                      <th>#</th>\r\n                      <th>Date</th>\r\n                      <th>Fee</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                    <tr>\r\n                      <td>01</td>\r\n                      <td>14-July-2017</td>\r\n                      <td>Rs2500</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>02</td>\r\n                      <td>12-July-2017</td>\r\n                      <td>Rs3000</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>03</td>\r\n                      <td>12-June-2017</td>\r\n                      <td>Rs1500</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>04</td>\r\n                      <td>01-May-2017</td>\r\n                      <td>Rs 1000</td>\r\n                    </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </ng-template>\r\n            </ngb-tab>\r\n            <ngb-tab>\r\n              <ng-template ngbTabTitle><b>2nd Year</b></ng-template>\r\n              <ng-template ngbTabContent>\r\n                <div class=\"row container\" style=\"margin-top:25px\">\r\n                  <div class=\"col-sm-4\"><b>Payed:</b> Rs 8000</div>\r\n                  <div class=\"col-sm-4 text-center\"><b>Remaining:</b> Rs 0</div>\r\n                  <div class=\"col-sm-4 text-right\"><b>Total Fee:</b> Rs8000</div>\r\n                </div>\r\n\r\n                <div class=\"table-responsive\" style=\"margin-top: 20px\">\r\n                  <table class=\"table table-bordered\">\r\n                    <thead>\r\n                    <tr>\r\n                      <th>#</th>\r\n                      <th>Date</th>\r\n                      <th>Fee</th>\r\n                    </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                    <tr>\r\n                      <td>01</td>\r\n                      <td>14-July-2017</td>\r\n                      <td>Rs2500</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>02</td>\r\n                      <td>12-July-2017</td>\r\n                      <td>Rs3000</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>03</td>\r\n                      <td>12-June-2017</td>\r\n                      <td>Rs1500</td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>04</td>\r\n                      <td>01-May-2017</td>\r\n                      <td>Rs 1000</td>\r\n                    </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n              </ng-template>\r\n            </ngb-tab>\r\n          </ngb-tabset>\r\n        </div>\r\n      </div>\r\n      <!-- End of Fee Details -->\r\n\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 260:
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <div class=\"card FMS-card\">\r\n    <div class=\"card-header FMS-card-header\">Students</div>\r\n    <div class=\"card-block\">\r\n      <!-- Filter -->\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <select name=\"class\" id=\"class\" class=\"form-control FMS-input\">\r\n            <option value=\"\">Select Class</option>\r\n            <option value=\"bsc\">B. Sc.</option>\r\n            <option value=\"ba\">B. A.</option>\r\n            <option value=\"ma\">M. A.</option>\r\n            <option value=\"bcom\">B. Com.</option>\r\n          </select>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <select name=\"year\" class=\"form-control FMS-input\" id=\"year\">\r\n            <option value=\"\">Select Year</option>\r\n            <option value=\"1\">1st Year</option>\r\n            <option value=\"2\">2nd Year</option>\r\n            <option value=\"3\">3rd Year</option>\r\n          </select>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <input type=\"text\" name=\"name\" id=\"name\" class=\"form-control FMS-input\" placeholder=\"Enter Student Name\">\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <input type=\"text\" name=\"father_name\" id=\"father_name\" class=\"form-control FMS-input\"\r\n                 placeholder=\"Enter Father Name\">\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <input type=\"text\" name=\"roll_number\" id=\"roll_number\" class=\"form-control FMS-input\"\r\n                 placeholder=\"Enter Roll Number\">\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-4\">\r\n          <input type=\"text\" name=\"mobile\" id=\"mobile\" class=\"form-control FMS-input\" placeholder=\"Enter Mobile Number\">\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row FMS-input-group\">\r\n        <div class=\"col-sm-12 text-right\">\r\n          <button type=\"button\" class=\"btn btn-primary\">Search</button>\r\n        </div>\r\n      </div>\r\n      <!-- End of Filter -->\r\n\r\n      <hr>\r\n\r\n      <div class=\"table-responsive\">\r\n        <table class=\"table table-bordered\">\r\n          <thead>\r\n          <tr>\r\n            <th>#</th>\r\n            <th>Photo</th>\r\n            <th>Details</th>\r\n            <th>Course</th>\r\n            <th>Action</th>\r\n          </tr>\r\n          </thead>\r\n          <tbody>\r\n          <tr *ngFor=\"let student of students; let i = index\">\r\n            <td>0{{i+1}}</td>\r\n            <td><img [src]=\"student.profileImage || '../../../assets/images/default_profile.png'\" [alt]=\"student.name\"\r\n                     style=\"width: 160px;\"></td>\r\n            <td>\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\">\r\n                  <strong>Name :</strong>\r\n                </div>\r\n                <div class=\"col-sm-9\">\r\n                  {{student.name}}\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\">\r\n                  <strong>Father :</strong>\r\n                </div>\r\n                <div class=\"col-sm-9\">\r\n                  {{student.fatherName}}\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\">\r\n                  <strong>Mother :</strong>\r\n                </div>\r\n                <div class=\"col-sm-9\">\r\n                  {{student.motherName}}\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\">\r\n                  <strong>DOB :</strong>\r\n                </div>\r\n                <div class=\"col-sm-9\">\r\n                  {{student.dob| date: 'dd-MMM-yy'}}\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\">\r\n                  <strong>Mobile :</strong>\r\n                </div>\r\n                <div class=\"col-sm-9\">\r\n                  {{student.mobile}}\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\">\r\n                  <strong>Email :</strong>\r\n                </div>\r\n                <div class=\"col-sm-9\">\r\n                  {{student.email}}\r\n                </div>\r\n              </div>\r\n\r\n            </td>\r\n            <td>\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\"><strong>Name :</strong></div>\r\n                <div class=\"col-sm-9\">{{student.class}}</div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\"><strong>Year :</strong></div>\r\n                <div class=\"col-sm-9\">{{student.year}}</div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\"><strong>Roll No :</strong></div>\r\n                <div class=\"col-sm-9\">{{student.rollNumber}}</div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n                <div class=\"col-sm-3\"><strong>Admission Date :</strong></div>\r\n                <div class=\"col-sm-9\">{{student.admissionDate | date: 'dd-MMM-yy'}}</div>\r\n              </div>\r\n            </td>\r\n            <td>\r\n              <button class=\"btn btn-link\"><i class=\"fa fa-edit\"></i></button>\r\n              <br>\r\n              <button class=\"btn btn-link text-danger\"><i class=\"fa fa-trash-o\"></i></button>\r\n              <br>\r\n              <br>\r\n              <button class=\"btn btn-success\">See Details</button>\r\n\r\n            </td>\r\n          </tr>\r\n\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 294:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(169);


/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ApiService = (function () {
    function ApiService(http) {
        this.http = http;
        this.URL = 'http://localhost:3000/api/';
    }
    // Retrieving Courses
    ApiService.prototype.getCourse = function () {
        return this.http.get(this.URL + 'course')
            .map(function (res) { return res.json(); });
    };
    // Add Course
    ApiService.prototype.addCourse = function (newCourse) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.URL + 'course', newCourse, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    // Delete Course
    ApiService.prototype.deleteCourse = function (id) {
        return this.http.delete(this.URL + 'course/' + id)
            .map(function (res) { return res.json(); });
    };
    // Update Course
    ApiService.prototype.updateCourse = function (id, course) {
        console.log(id);
        console.log(course);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]();
        headers.append('Content-Type', 'application/json');
        return this.http.put(this.URL + 'course/' + id, course, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    return ApiService;
}());
ApiService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Http */]) === "function" && _a || Object])
], ApiService);

var _a;
//# sourceMappingURL=api.service.js.map

/***/ })

},[294]);
//# sourceMappingURL=main.bundle.js.map